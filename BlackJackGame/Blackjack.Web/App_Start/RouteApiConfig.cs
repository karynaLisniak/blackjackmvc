﻿using System.Web.Http;
using System.Web.Mvc;

namespace Blackjack.Web
{
    public class RouteApiConfig
    {
        public static void RegisterRoutes(HttpRouteCollection routes)
        {
            routes.IgnoreRoute("resources", "{resource}.axd/{*pathInfo}");

            routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
