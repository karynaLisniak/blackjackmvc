﻿using Autofac;
using Autofac.Extras.NLog;
using Autofac.Integration.WebApi;
using AutoMapper;
using Blackjack.BusinessLogic.Autofac;
using Blackjack.BusinessLogic.AutoMapper;
using Blackjack.Web.Filters;
using System.Web.Configuration;
using System.Web.Http;

namespace Blackjack.Web
{
    public class AutofacConfig
    {
        public static IContainer ConfigureContainer(HttpConfiguration httpConfiguration)
        {
            ContainerBuilder builder = new ContainerBuilder();
            
            string connectionString = WebConfigurationManager.ConnectionStrings["DbConnection"].ConnectionString;
            builder.RegisterModule(new BusinessLogicModule(connectionString));
            
            builder.RegisterApiControllers(typeof(Startup).Assembly);

            RegisterLogger(builder, httpConfiguration);
            RegisterMapperProfile(builder);

            return builder.Build();
        }

        private static void RegisterLogger(ContainerBuilder builder, HttpConfiguration config)
        {
            builder.RegisterWebApiFilterProvider(config);
            builder.RegisterModule<NLogModule>();
            builder.RegisterType<LoggerFilter>().AsWebApiExceptionFilterFor<ApiController>();
        }

        private static void RegisterMapperProfile(ContainerBuilder builder)
        {
            MapperConfiguration mapperConfiguration =
                new MapperConfiguration(cfg => cfg.AddProfile(new MapperProfile()));
            IMapper mapper = mapperConfiguration.CreateMapper();
            builder.Register(c => mapper).As<IMapper>().SingleInstance();
        }
    }
}