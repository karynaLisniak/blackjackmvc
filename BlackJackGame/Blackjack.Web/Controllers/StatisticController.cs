﻿using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.ViewModels.Common;
using Blackjack.ViewModels.Statistic;
using System.Threading.Tasks;
using System.Web.Http;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;

namespace Blackjack.Web.Controllers
{
    public class StatisticController : ApiController
    {
        private readonly IStatisticService _statisticService;

        public StatisticController(IStatisticService statisticService)
        {
            _statisticService = statisticService;
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> GetRound([FromBody] GetRoundStatisticViewModel requestModel)
        {
            RoundViewModel round = await _statisticService.GetRoundById(requestModel);
            return Ok(round);
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> GetSearchResultRounds([FromBody] GetSearchResultRoundsStatisticViewModel requestModel)
        {
            SearchResultRoundsViewModel responseModel = await _statisticService.GetRounds(requestModel);
            return Ok(responseModel);
        }
    }
}