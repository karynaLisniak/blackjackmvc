﻿using System.Web.Mvc;

namespace Blackjack.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return new FilePathResult("~/Scripts/dist/index.html", "text/html");
        }
    }
}