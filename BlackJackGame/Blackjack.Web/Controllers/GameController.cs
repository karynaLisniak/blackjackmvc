﻿using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.ViewModels.Common;
using Blackjack.ViewModels.Game;
using System.Threading.Tasks;
using System.Web.Http;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;

namespace Blackjack.Web.Controllers
{
    public class GameController : ApiController
    {
        private readonly IGameService _gameService;

        public GameController(IGameService gameService)
        {
            _gameService = gameService;
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> Start([FromBody] RequestStartGameViewModel requestModel)
        {
            ResponseGameInfoViewModel responseStartGameModel = await _gameService.Start(requestModel);
            return Ok(responseStartGameModel);
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> Deal([FromBody] DealGameViewModel requestModel)
        {
            RoundViewModel responseModel = await _gameService.Deal(requestModel);
            return Ok(responseModel);
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> Hit([FromBody] HitGameViewModel requestModel)
        {
            RoundViewModel responseHitModel = await _gameService.Hit(requestModel);
            if (responseHitModel == null)
            {
                return BadRequest();
            }

            return Ok(responseHitModel);
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> Stand([FromBody] StandGameViewModel requestModel)
        {
            RoundViewModel responseModel = await _gameService.Stand(requestModel);
            if (responseModel == null)
            {
                return BadRequest();
            }

            return Ok(responseModel);
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> GetPlayer([FromBody] GetPlayerGameViewModel requestModel)
        {
            PlayerViewModel responseModel = await _gameService.GetPlayerModel(requestModel);
            return Ok(responseModel);
        }
    }
}