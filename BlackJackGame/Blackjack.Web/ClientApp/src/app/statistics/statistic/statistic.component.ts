import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SortDescriptor, orderBy } from '@progress/kendo-data-query';
import { GridDataResult, PageChangeEvent } from '@progress/kendo-angular-grid';

import { StatisticService } from 'src/app/shared/services/statistic.service';
import { Round } from 'src/app/shared/models/round/round.model';
import { ModalComponent } from 'src/app/statistics/modal/modal.component';
import { GetSearchResultRoundsView } from 'src/app/shared/models/statistic/get.search.result.round.statistic.model';
import { GetRoundView } from 'src/app/shared/models/statistic/get.round.statistic.model';

@Component({
  selector: 'app-statistic',
  templateUrl: './statistic.component.html',
  styleUrls: ['./statistic.component.css']
})
export class StatisticComponent implements OnInit {

  private gridView: GridDataResult;
  private data: Round[];
  private total: number;
  private pageSize: number = 5;
  private skip: number = 0;
  private sort: SortDescriptor[] = [{
    field: 'player',
    dir: 'asc'
  }];

  private playerName: string;
  private selectedData: Round;

  constructor(private modalService: NgbModal, private statisticService: StatisticService) {

  }

  ngOnInit() {

  }

  public pageChange(event: PageChangeEvent) {
    this.skip = event.skip;
    this.getRounds(this.playerName);
  }

  public sortChange(sort: SortDescriptor[]) {
    this.sort = sort;
    this.loadItems();
  }

  public gridSelectionChange(selection) {
    this.selectedData = selection.selectedRows[0].dataItem;
  }

  public getRounds(playerName: string = "") {
    var getRondsView = new GetSearchResultRoundsView();
    getRondsView.playerName = playerName;
    getRondsView.startPosition = this.skip;
    getRondsView.countRounds = this.pageSize;

    this.statisticService.getRounds(getRondsView)
      .subscribe(response => {
        this.data = response.rounds;
        this.total = response.countRounds;

        this.loadItems();
      },
        error => console.log(error)
      );
  }

  public getRoundById() {
    var getRoundView = new GetRoundView();
    getRoundView.roundId = this.selectedData.roundId;

    this.statisticService.getRound(getRoundView)
      .subscribe(roundData => {
        const modalRef = this.modalService.open(ModalComponent);
        modalRef.componentInstance.round = roundData;
      },
        error => console.log(error)
      );
  }

  private loadItems() {
    this.gridView = {
      data: orderBy(this.data, this.sort),
      total: this.total
    }
  }

}
