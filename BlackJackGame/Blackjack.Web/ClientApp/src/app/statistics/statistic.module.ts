import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GridModule } from '@progress/kendo-angular-grid';

import { StatisticsRoutingModule } from 'src/app/statistics/statistics-routing.module'

import { StatisticComponent } from 'src/app/statistics/statistic/statistic.component';
import { ModalComponent } from 'src/app/statistics/modal/modal.component';

@NgModule({
  declarations: [
    StatisticComponent,
    ModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    GridModule,
    NgbModule.forRoot(),
    StatisticsRoutingModule
  ],
  exports: [
    StatisticComponent,
    ModalComponent
  ],
  entryComponents: [ModalComponent],
  providers: [HttpClientModule]
})
export class StatisticModule { }
