import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { Round } from 'src/app/shared/models/round/round.model';
import { Player } from 'src/app/shared/models/round/player.model';
import { DealGameView } from 'src/app/shared/models/game/deal.game.model';
import { GetPlayerGameView } from 'src/app/shared/models/game/get.player.game.model';
import { HitGameView } from 'src/app/shared/models/game/hit.game.model';
import { StandGameView } from 'src/app/shared/models/game/stand.game.model';

import { GameService } from 'src/app/shared/services/game.service';
import { WrapperService } from 'src/app/shared/services/wrapper.service';

import { RoundStatus } from 'src/app/shared/enums/round.status.enum';

@Component({
  selector: 'app-game-table',
  templateUrl: './game-table.component.html',
  styleUrls: ['./game-table.component.css']
})

export class GameTableComponent implements OnInit {

  private round = new Round();
  private isRoundResult = false;

  roundForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private gameService: GameService,
    private wrapper: WrapperService) {

  }

  ngOnInit() {
    this.initForm();
    this.getPlayer();
  }

  initForm() {
    this.roundForm = this.formBuilder.group({
      rate: [null, [
        Validators.required,
        Validators.pattern(/\b[1-9]*\b/)
      ]],
      countBots: [null, [
        Validators.pattern(/\b[0-9]+\d{0,1}\b/)
      ]]
    });
  }

  public getPlayer() {
    var getPlayerView = new GetPlayerGameView();
    getPlayerView.playerId = this.wrapper.getPlayerId();

    this.gameService.getPlayer(getPlayerView)
      .subscribe(player => {
        this.round.player = player;
      },
        error => console.log(error)
      );
  }

  public startRound() {
    var dealView = this.getDealView();
    this.isRoundResult = false;

    this.gameService.startRound(dealView)
      .subscribe(roundData => {
        this.wrapper.setRoundId(roundData.roundId);
        this.round = roundData;

        if (roundData.status != RoundStatus[RoundStatus.NotFinished]) {
          this.round.status = roundData.status;
          this.isRoundResult = true;
        }
      },
        error => console.log(error)
      );
  }

  public hit() {
    var hitView = new HitGameView();
    hitView.roundId = this.wrapper.getRoundId();

    this.gameService.hit(hitView)
      .subscribe(roundData => {
        this.addPlayersCards(roundData);

        if (roundData.status != RoundStatus[RoundStatus.NotFinished]) {
          this.round.status = roundData.status;
          this.isRoundResult = true;
        }
      },
        error => console.log(error)
      );
  }

  public stand() {
    var standView = new StandGameView();
    standView.roundId = this.wrapper.getRoundId();

    this.gameService.stand(standView)
      .subscribe(roundData => {
        this.addPlayersCards(roundData);

        this.round.status = roundData.status;
        this.isRoundResult = true;
      },
        error => console.log(error)
      );
  }

  private getDealView() {
    var dealView = new DealGameView();
    dealView.playerId = this.wrapper.getPlayerId();
    dealView.gameId = this.wrapper.getGameId();
    dealView.rate = this.roundForm.value.rate;
    dealView.countBots = this.roundForm.value.countBots;
    return dealView;
  }

  private addPlayersCards(round: Round) {
    this.round.player.score = round.player.score;
    this.round.player.cards = this.round.player.cards.concat(round.player.cards);

    this.round.dealer.score = round.dealer.score;
    this.round.dealer.cards = this.round.dealer.cards.concat(round.dealer.cards);

    for (var index = 0; index < round.bots.length; index++) {
      var bot = this.findBot(round.bots[index].name);
      bot.score = round.bots[index].score;
      bot.cards = bot.cards.concat(round.bots[index].cards);
    }
  }

  private findBot(botName: string): Player {
    return this.round.bots.find((bot) => {
      if (bot.name == botName) {
        return true;
      }
      return false;
    })
  }
}

