import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameTableComponent } from 'src/app/game/game-table/game-table.component'

const routes: Routes = [
  {
    path: '',
    component: GameTableComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class GameRoutingModule { }
