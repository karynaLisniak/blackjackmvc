import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { GameService } from 'src/app/shared/services/game.service';
import { WrapperService } from 'src/app/shared/services/wrapper.service';

import { StartGameView } from 'src/app/shared/models/game/start.game.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  playerForm: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private gameService: GameService,
    private wrapper: WrapperService) {

  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.playerForm = this.formBuilder.group({
      playerName: [null, [
        Validators.required
      ]],
      cash: [null]
    });
  }

  public startGame() {
    var player = this.getPlayerModel();

    this.gameService.startGame(player)
      .subscribe(startGameModel => {
          this.wrapper.setPlayerId(startGameModel.playerId);
          this.wrapper.setGameId(startGameModel.gameId);

          this.router.navigate(['/gameTable']);
        },
        error => console.log(error)
      );
  }

  public viewStatistics() {
    this.router.navigate(['/statistic']);
  }

  private getPlayerModel() {
    var startGameView = new StartGameView();
    startGameView.name = this.playerForm.value.playerName;
    startGameView.cash = this.playerForm.value.cash;

    return startGameView;
  }

}
