import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from 'src/app/home/home.component';

const routes: Routes = [
  { 
    path: '', 
    redirectTo: '/home', pathMatch: 'full' 
  },
  { 
    path: 'home', 
    component: HomeComponent 
  },
  { 
    path: 'gameTable', 
    loadChildren: 'src/app/game/game.module#GameModule' 
  },
  {
    path: 'statistic',
    loadChildren: 'src/app/statistics/statistic.module#StatisticModule'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {

}
