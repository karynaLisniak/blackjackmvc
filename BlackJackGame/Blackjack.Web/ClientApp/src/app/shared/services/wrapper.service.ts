import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WrapperService {

  constructor() { }

  public setGameId(gameId: number) {
    localStorage.setItem('gameId', gameId.toString());
  }

  public getGameId() {
    var gameId = localStorage.getItem('gameId');
    return parseInt(gameId);
  }

  public setRoundId(roundId: number) {
    localStorage.setItem('roundId', roundId.toString());
  }

  public getRoundId() {
    var roundId = localStorage.getItem('roundId');
    return parseInt(roundId);
  }

  public setPlayerId(playerId: number) {
    localStorage.setItem('playerId', playerId.toString());
  }

  public getPlayerId() {
    var playerId = localStorage.getItem('playerId');
    return parseInt(playerId);
  }
}
