import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment'

import { Round } from 'src/app/shared/models/round/round.model';
import { SearchResultRounds } from 'src/app/shared/models/statistic/search.result.rounds.model';
import { GetSearchResultRoundsView } from 'src/app/shared/models/statistic/get.search.result.round.statistic.model';
import { GetRoundView } from 'src/app/shared/models/statistic/get.round.statistic.model';

@Injectable({
  providedIn: 'root'
})
export class StatisticService {

  private url = environment.ApiStatisticEndpoint;

  constructor(private http: HttpClient) {

  }

  getRounds(getRoundsView: GetSearchResultRoundsView): Observable<SearchResultRounds> {
    return this.http
      .post<SearchResultRounds>((`${this.url}getSearchResultRounds`), getRoundsView);
  }

  getRound(getRoundView: GetRoundView): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}getRound`), getRoundView);
  }
}
