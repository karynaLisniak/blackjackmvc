import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment'

import { Player } from 'src/app/shared/models/round/player.model';
import { Round } from 'src/app/shared/models/round/round.model';
import { GameInfoView } from 'src/app/shared/models/game/game.info.model'
import { StartGameView } from 'src/app/shared/models/game/start.game.model';
import { DealGameView } from 'src/app/shared/models/game/deal.game.model';
import { HitGameView } from 'src/app/shared/models/game/hit.game.model';
import { StandGameView } from 'src/app/shared/models/game/stand.game.model';
import { GetPlayerGameView } from 'src/app/shared/models/game/get.player.game.model';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private url = environment.ApiGameEndpoint;

  constructor(private http: HttpClient) {

  }

  startGame(startGameView: StartGameView): Observable<GameInfoView> {
    return this.http
      .post<GameInfoView>((`${this.url}start`), startGameView);
  }

  startRound(dealView: DealGameView): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}deal`), dealView);
  }

  hit(hitView: HitGameView): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}hit`), hitView);
  }

  stand(standView: StandGameView): Observable<Round> {
    return this.http
      .post<Round>((`${this.url}stand`), standView);
  }

  getPlayer(getPlayerView: GetPlayerGameView): Observable<Player> {
    return this.http
      .post<Player>((`${this.url}getPlayer`), getPlayerView);
  }

}
