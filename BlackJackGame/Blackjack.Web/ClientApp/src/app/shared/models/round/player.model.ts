import { Card } from "src/app/shared/models/round/card.model";

export class Player {
  id: number;
  role: string;
  name: string;
  cash: number;
  isPlayed: boolean;
  rate: number;
  cards: Card[];
  score: number;

  public constructor() {
    this.score = 0;
    this.cards = [];
  }
}
