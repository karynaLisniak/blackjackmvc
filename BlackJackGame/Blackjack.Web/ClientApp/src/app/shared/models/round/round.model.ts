import { Player } from "src/app/shared/models/round/player.model";

export class Round {
  roundId: number;
  player: Player;
  bots: Player[];
  dealer: Player;
  status: string;

  public constructor() {
    this.player = new Player();
    this.bots = [];
    this.dealer = new Player();
  }
}
