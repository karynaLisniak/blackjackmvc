export class DealGameView {
    public gameId: number;
    public playerId: number;
    public countBots: number;
    public rate: number;
}