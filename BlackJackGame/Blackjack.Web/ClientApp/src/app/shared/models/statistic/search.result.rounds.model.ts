import { Round } from 'src/app/shared/models/round/round.model';

export class SearchResultRounds {
    rounds: Round[];
    countRounds: number;
}