export const environment = {
  production: true,
  ApiGameEndpoint: "/api/game/",
  ApiStatisticEndpoint: "/api/statistic/"
};
