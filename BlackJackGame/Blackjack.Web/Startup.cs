﻿using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.Owin;
using Owin;
using System.Web.Http;
using System.Web.Routing;

[assembly: OwinStartup(typeof(Blackjack.Web.Startup))]

namespace Blackjack.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = GlobalConfiguration.Configuration;

            IContainer container = AutofacConfig.ConfigureContainer(config);
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            WebConfig.Register(config);

            RouteApiConfig.RegisterRoutes(config.Routes);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}