﻿using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Autofac.Extras.NLog;
using Autofac.Integration.WebApi;

namespace Blackjack.Web.Filters
{
    public class LoggerFilter : IAutofacExceptionFilter
    {
        private readonly ILogger _logger;

        public LoggerFilter(ILogger logger)
        {
            _logger = logger;
        }

        public Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            _logger.Error(actionExecutedContext.Exception);
            return  Task.CompletedTask;
        }
    }
}