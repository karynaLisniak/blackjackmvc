﻿using AutoMapper;
using Blackjack.BusinessLogic.Helpers;
using Blackjack.BusinessLogic.Managers.Interfaces;
using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.Entities.Enums;
using Blackjack.ViewModels.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers
{
    public class PlayerManager : IPlayerManager
    {
        private readonly IMapper _mapper;
        private readonly IPlayerRepository _playerRepository;
        private readonly IPlayerRoundRepository _playerRoundRepository;

        public PlayerManager(
            IMapper mapper,
            IPlayerRepository playerRepository,
            IPlayerRoundRepository playerRoundRepository)
        {
            _mapper = mapper;
            _playerRepository = playerRepository;
            _playerRoundRepository = playerRoundRepository;
        }

        public async Task<PlayerViewModel> AuthorizeUser(string playerName, double playerCash)
        {
            Player user = await _playerRepository.GetByNameAndRole(playerName);
            if (user == null)
            {
                user = CreatePlayer(playerName, PlayerRole.User, playerCash);
                user = await _playerRepository.Create(user);
            }

            PlayerViewModel playerModel = _mapper.Map<Player, PlayerViewModel>(user);
            return playerModel;
        }

        public async Task<RoundViewModel> CreateRoundBotsAndDealer(RoundViewModel roundModel, int countBots)
        {
            List<Player> players = CreateBots(countBots);
            Player dealer = CreatePlayer("Dealer", PlayerRole.Dealer);
            players.Add(dealer);

            players = await _playerRepository.Create(players);

            roundModel.Bots = _mapper.Map<List<Player>, List<PlayerViewModel>>(players.Where(player => player.Role == PlayerRole.Bot).ToList());
            roundModel.Dealer = _mapper.Map<Player, PlayerViewModel>(dealer);
            return roundModel;
        }

        public async Task CreatePlayersRounds(IList<PlayerViewModel> playerModels, long roundId)
        {
            List<PlayerRound> playerRounds = playerModels.SelectMany(player => player.Cards
                .Select(card => new PlayerRound
                {
                    PlayerId = player.Id,
                    RoundId = roundId,
                    CardId = card.Id,
                    CardPoints = card.Points
                })).ToList();

            await _playerRoundRepository.Create(playerRounds);
        }

        public async Task<PlayerViewModel> GetPlayerById(long playerId)
        {
            Player player = await _playerRepository.GetById(playerId);

            PlayerViewModel playerModel = _mapper.Map<Player, PlayerViewModel>(player);
            return playerModel;
        }

        public PlayerViewModel GivePlayerCards(PlayerViewModel player, IList<CardViewModel> freeCards, int countCards)
        {
            int playerTotalPoints = RecieverPlayerScoreHelper.GetPlayerTotalPoints(player.Role);

            for (int index = 0; index < countCards; index++)
            {
                if (player.Score < playerTotalPoints)
                {
                    player = AddPlayerCard(player, playerTotalPoints, freeCards);
                }
            }
            return player;
        }

        public PlayerViewModel GivePlayerAllCards(PlayerViewModel player, IList<CardViewModel> freeCards)
        {
            int playerTotalPoints = RecieverPlayerScoreHelper.GetPlayerTotalPoints(player.Role);

            while (player.Score < playerTotalPoints)
            {
                player = AddPlayerCard(player, playerTotalPoints, freeCards);
            }
            return player;
        }

        private Player CreatePlayer(string playerName, PlayerRole role, double playerCash = 0)
        {
            Player player = new Player
            {
                Name = playerName,
                Role = role,
                Cash = playerCash
            };
            return player;
        }

        private List<Player> CreateBots(int countBots)
        {
            List<Player> bots = new List<Player>();

            for (int index = 1; index <= countBots; index++)
            {
                Player bot = CreatePlayer($"Bot{index}", PlayerRole.Bot);
                bots.Add(bot);
            }
            return bots;
        }

        private PlayerViewModel AddPlayerCard(PlayerViewModel player, int winnerScore, IList<CardViewModel> freeCards)
        {
            CardViewModel card = RecieverCardDataHelper.GetFreeCard(freeCards);

            int cardPoints = RecieverCardDataHelper.GetCardPoint(card, player.Score, winnerScore);
            card.Points = cardPoints;
            player.Score += cardPoints;

            player.Cards.Add(card);
            return player;
        }
    }
}