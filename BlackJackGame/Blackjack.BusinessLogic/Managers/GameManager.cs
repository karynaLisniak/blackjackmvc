﻿using AutoMapper;
using Blackjack.BusinessLogic.Managers.Interfaces;
using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.ViewModels.Game;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers
{
    public class GameManager : IGameManager
    {
        private readonly IMapper _mapper;
        private readonly IGameRepository _gameRepository;

        public GameManager(
            IMapper mapper,
            IGameRepository gameRepository)
        {
            _mapper = mapper;
            _gameRepository = gameRepository;
        }

        public async Task<ResponseGameInfoViewModel> CreateGame()
        {
            Game game = new Game();
            game = await _gameRepository.Create(game);

            ResponseGameInfoViewModel gameModel = _mapper.Map<Game, ResponseGameInfoViewModel>(game);
            return gameModel;
        }
    }
}