﻿using AutoMapper;
using Blackjack.BusinessLogic.Managers.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.ViewModels.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blackjack.DataAccess.Repositories.Interfaces;

namespace Blackjack.BusinessLogic.Managers
{
    public class CardManager : ICardManager
    {
        private readonly IMapper _mapper;
        private readonly ICardRepository _cardRepository;

        public CardManager(
            IMapper mapper,
            ICardRepository cardRepository)
        {
            _mapper = mapper;
            _cardRepository = cardRepository;
        }

        public async Task<List<CardViewModel>> GetFreeCards(long roundId)
        {
            List<Card> cards = await _cardRepository.GetFreeCards(roundId);

            List<CardViewModel> cardModels = _mapper.Map<List<Card>, List<CardViewModel>>(cards);
            return cardModels;
        }
    }
}