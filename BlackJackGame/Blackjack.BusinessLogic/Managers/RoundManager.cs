﻿using AutoMapper;
using Blackjack.BusinessLogic.Helpers;
using Blackjack.BusinessLogic.Managers.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.Entities.Enums;
using Blackjack.ViewModels.Common;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blackjack.DataAccess.Repositories.Interfaces;

namespace Blackjack.BusinessLogic.Managers
{
    public class RoundManager : IRoundManager
    {
        private readonly IMapper _mapper;
        private readonly IRoundRepository _roundRepository;
        private readonly IPlayerRepository _playerRepository;

        public RoundManager(
            IMapper mapper,
            IRoundRepository roundRepository,
            IPlayerRepository playerRepository)
        {
            _mapper = mapper;
            _roundRepository = roundRepository;
            _playerRepository = playerRepository;
        }

        public async Task<RoundViewModel> CreateRound(long gameId, double rate)
        {
            Round round = new Round
            {
                GameId = gameId,
                Rate = rate,
                Status = RoundStatus.NotFinished,
                PlayerRounds = new List<PlayerRound>()
            };
            round = await _roundRepository.Create(round);

            RoundViewModel roundModel = _mapper.Map<Round, RoundViewModel>(round);
            return roundModel;
        }

        public async Task<RoundViewModel> ChangeRoundStatus(RoundViewModel roundModel)
        {
            PlayerViewModel user = roundModel.Player;
            PlayerViewModel dealer = roundModel.Dealer;

            if (user.Score == dealer.Score && user.Score <= Constant.BLACKJACK)
            {
                roundModel.Status = RoundStatus.Push.ToString();
            }
            if (user.Score > Constant.BLACKJACK && dealer.Score > Constant.BLACKJACK)
            {
                roundModel.Status = RoundStatus.Draw.ToString();
            }
            if (IsDealerWinner(user.Score, dealer.Score)
                || IsPlayerBust(user.Score, dealer.Score))
            {
                roundModel.Status = RoundStatus.DealerWon.ToString();
                user.Cash -= user.Rate;
            }
            if (IsPlayerWinner(user.Score, user.Cards.Count, dealer.Score)
                || IsDealerBust(user.Score, dealer.Score, user.Cards.Count))
            {
                roundModel.Status = RoundStatus.PlayerWon.ToString();
                user.Cash += user.Rate * Constant.COEFFICIENT_PLAYER_WIN_RATE;
            }
            if (user.Score == Constant.BLACKJACK && user.Cards.Count == Constant.COUNT_BLACKJACK_CARDS)
            {
                roundModel.Status = RoundStatus.Blackjack.ToString();
                user.Cash += user.Rate * Constant.COEFFICIENT_BLACKJACK_RATE;
            }

            Round round = _mapper.Map<RoundViewModel, Round>(roundModel);
            Player player = _mapper.Map<PlayerViewModel, Player>(user);
            await _roundRepository.Update(round);
            await _playerRepository.Update(player);
            return roundModel;
        }

        public async Task<RoundViewModel> GetRound(long roundId)
        {
            Round round = await _roundRepository.GetById(roundId);

            RoundViewModel roundModel = _mapper.Map<Round, RoundViewModel>(round);
            return roundModel;
        }

        private bool IsDealerWinner(int userScore, int dealerScore)
        {
            return (userScore < dealerScore && dealerScore <= Constant.BLACKJACK);
        }

        private bool IsDealerBust(int userScore, int dealerScore, int userCardsCount)
        {
            return (userScore <= Constant.BLACKJACK && dealerScore > Constant.BLACKJACK && userCardsCount != 2);
        }

        private bool IsPlayerWinner(int userScore, int userCardsCount, int dealerScore)
        {
            return (dealerScore < userScore && userScore <= Constant.BLACKJACK && userCardsCount != 2);
        }

        private bool IsPlayerBust(int userScore, int dealerScore)
        {
            return (dealerScore <= Constant.BLACKJACK && userScore > Constant.BLACKJACK);
        }
    }
}