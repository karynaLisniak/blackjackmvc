﻿using Blackjack.ViewModels.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers.Interfaces
{
    public interface IPlayerManager
    {
        Task<PlayerViewModel> AuthorizeUser(string playerName, double playerCash);

        Task<RoundViewModel> CreateRoundBotsAndDealer(RoundViewModel roundModel, int countBots);

        Task CreatePlayersRounds(IList<PlayerViewModel> playerModels, long roundId);

        Task<PlayerViewModel> GetPlayerById(long playerId);

        PlayerViewModel GivePlayerCards(PlayerViewModel player, IList<CardViewModel> freeCards, int countCards);

        PlayerViewModel GivePlayerAllCards(PlayerViewModel player, IList<CardViewModel> freeCards);
    }
}