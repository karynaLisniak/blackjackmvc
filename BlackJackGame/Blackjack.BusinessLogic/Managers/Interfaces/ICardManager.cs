﻿using Blackjack.ViewModels.Common;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers.Interfaces
{
    public interface ICardManager
    {
        Task<List<CardViewModel>> GetFreeCards(long roundId);
    }
}