﻿using Blackjack.ViewModels.Game;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers.Interfaces
{
    public interface IGameManager
    {
        Task<ResponseGameInfoViewModel> CreateGame();
    }
}