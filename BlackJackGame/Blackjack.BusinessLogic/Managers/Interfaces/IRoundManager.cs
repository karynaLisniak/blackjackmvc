﻿using Blackjack.ViewModels.Common;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Managers.Interfaces
{
    public interface IRoundManager
    {
        Task<RoundViewModel> CreateRound(long gameId, double rate);

        Task<RoundViewModel> ChangeRoundStatus(RoundViewModel round);

        Task<RoundViewModel> GetRound(long roundId);
    }
}