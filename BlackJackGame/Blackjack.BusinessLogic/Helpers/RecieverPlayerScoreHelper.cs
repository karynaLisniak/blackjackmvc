﻿using Blackjack.Entities.Enums;

namespace Blackjack.BusinessLogic.Helpers
{
    public static class RecieverPlayerScoreHelper
    {
        public static int GetPlayerTotalPoints(string role)
        {
            if (role == PlayerRole.User.ToString())
            {
                return Constant.BLACKJACK;
            }

            if (role == PlayerRole.Bot.ToString())
            {
                return Constant.BOT_POINT;
            }

            if (role == PlayerRole.Dealer.ToString())
            {
                return Constant.DEALER_POINT;
            }

            return 0;
        }
    }
}