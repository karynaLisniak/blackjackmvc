﻿namespace Blackjack.BusinessLogic.Helpers
{
    public static class Constant
    {
        public const int BLACKJACK = 21;
        public  const int BOT_POINT = 19;
        public const int DEALER_POINT = 17;
        
        public const int ACE_CARD_POINTS = 1;

        public const int COUNT_BLACKJACK_CARDS = 2;

        public const double COEFFICIENT_PLAYER_WIN_RATE = 2;
        public const double COEFFICIENT_BLACKJACK_RATE = 1.5;
    }
}