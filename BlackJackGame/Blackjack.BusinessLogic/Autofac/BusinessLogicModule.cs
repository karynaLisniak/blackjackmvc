﻿using Autofac;
using Blackjack.DataAccess.Autofac;
using System.Reflection;
using Module = Autofac.Module;

namespace Blackjack.BusinessLogic.Autofac
{
    public class BusinessLogicModule : Module
    {
        private string ConnectionString { get; }

        public BusinessLogicModule(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var businessLayerAssembly = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(businessLayerAssembly)
                .Where(t => t.Name.EndsWith("Service"))
                .AsImplementedInterfaces();

            builder.RegisterAssemblyTypes(businessLayerAssembly)
                .Where(t => t.Name.EndsWith("Manager"))
                .AsImplementedInterfaces();

            builder.RegisterModule(new DataAccessModule(ConnectionString));
        }
    }
}