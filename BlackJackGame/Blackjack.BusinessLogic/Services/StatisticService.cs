﻿using AutoMapper;
using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.ViewModels.Common;
using Blackjack.ViewModels.Statistic;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly IMapper _mapper;
        private readonly IRoundRepository _roundRepository;

        public StatisticService(
            IMapper mapper,
            IRoundRepository roundRepository)
        {
            _mapper = mapper;
            _roundRepository = roundRepository;
        }

        public async Task<RoundViewModel> GetRoundById(GetRoundStatisticViewModel requestModel)
        {
            Round round = await _roundRepository.GetWithCardsById(requestModel.RoundId);

            RoundViewModel responseModel = _mapper.Map<Round, RoundViewModel>(round);
            return responseModel;
        }

        public async Task<SearchResultRoundsViewModel> GetRounds(GetSearchResultRoundsStatisticViewModel requestModel)
        {
            List<Round> rounds = await _roundRepository.GetByCount(requestModel.StartPosition, requestModel.CountRounds, requestModel.PlayerName);
            int countRounds = await _roundRepository.GetRoundsCount(requestModel.PlayerName);

            List<RoundViewModel> roundModels = _mapper.Map<List<Round>, List<RoundViewModel>>(rounds);
            SearchResultRoundsViewModel responseModel = new SearchResultRoundsViewModel
            {
                Rounds = roundModels,
                CountRounds = countRounds
            };
            return responseModel;
        }
    }
}