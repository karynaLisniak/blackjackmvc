﻿using System;
using Blackjack.BusinessLogic.Helpers;
using Blackjack.BusinessLogic.Managers.Interfaces;
using Blackjack.BusinessLogic.Services.Interfaces;
using Blackjack.Entities.Enums;
using Blackjack.ViewModels.Common;
using Blackjack.ViewModels.Game;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Services
{
    public class GameService : IGameService
    {
        private readonly IGameManager _gameManager;
        private readonly IPlayerManager _playerService;
        private readonly IRoundManager _roundService;
        private readonly ICardManager _cardService;

        public GameService(
            IGameManager gameService,
            IPlayerManager playerService,
            IRoundManager roundService,
            ICardManager cardService)
        {
            _gameManager = gameService;
            _playerService = playerService;
            _roundService = roundService;
            _cardService = cardService;
        }

        public async Task<ResponseGameInfoViewModel> Start(RequestStartGameViewModel requestModel)
        {
            PlayerViewModel userModel = await _playerService.AuthorizeUser(requestModel.Name, requestModel.Cash);

            ResponseGameInfoViewModel gameModel = await _gameManager.CreateGame();
            gameModel.PlayerId = userModel.Id;
            
            return gameModel;
        }

        public async Task<RoundViewModel> Deal(DealGameViewModel requestModel)
        {
            RoundViewModel round = await _roundService.CreateRound(requestModel.GameId, requestModel.Rate);

            round = await _playerService.CreateRoundBotsAndDealer(round, requestModel.CountBots);
            round.Player = await _playerService.GetPlayerById(requestModel.PlayerId);

            List<PlayerViewModel> players = GetRoundPlayers(round);
            round = await Play(round, players, 2);

            return round;
        }

        public async Task<RoundViewModel> Hit(HitGameViewModel requestModel)
        {
            RoundViewModel round = await _roundService.GetRound(requestModel.RoundId);
            List<PlayerViewModel> players = GetRoundPlayers(round)
                .Where(player => player.Role != PlayerRole.Dealer.ToString()).ToList();

            if (round.Status == RoundStatus.NotFinished.ToString())
            {
                round = await Play(round, players, 1);
            }
            return round;
        }

        public async Task<RoundViewModel> Stand(StandGameViewModel requestModel)
        {
            RoundViewModel round = await _roundService.GetRound(requestModel.RoundId);

            if (round.Status == RoundStatus.NotFinished.ToString())
            {
                round = await EndRound(round);
            }
            return round;
        }

        public async Task<PlayerViewModel> GetPlayerModel(GetPlayerGameViewModel requestModel)
        {
            PlayerViewModel playerModel = await _playerService.GetPlayerById(requestModel.PlayerId);
            return playerModel;
        }

        private List<PlayerViewModel> GetRoundPlayers(RoundViewModel round)
        {
            List<PlayerViewModel> players = new List<PlayerViewModel> { round.Player, round.Dealer };
            players.AddRange(round.Bots);

            return players;
        }

        private async Task<RoundViewModel> Play(RoundViewModel round, IList<PlayerViewModel> players, int countCards)
        {
            List<CardViewModel> freeCards = await _cardService.GetFreeCards(round.RoundId);
            foreach (var player in players)
            {
                _playerService.GivePlayerCards(player, freeCards, countCards);
            }
            
            if (round.Player.Score >= Constant.BLACKJACK)
            {
                round = await EndRound(round, freeCards);
            }
            if (round.Player.Score < Constant.BLACKJACK)
            {
                await _playerService.CreatePlayersRounds(players, round.RoundId);
            }
            return round;
        }

        private async Task<RoundViewModel> EndRound(RoundViewModel round, IList<CardViewModel> freeCards = null)
        {
            freeCards = freeCards ?? await _cardService.GetFreeCards(round.RoundId);

            List<PlayerViewModel> players = GetRoundPlayers(round);
            List<PlayerViewModel> bots = players.Where(player => player.Role != PlayerRole.User.ToString()).ToList();
            foreach (var bot in bots)
            {
                _playerService.GivePlayerAllCards(bot, freeCards);
            }

            await _playerService.CreatePlayersRounds(players, round.RoundId);
            round = await _roundService.ChangeRoundStatus(round);
            return round;
        }
    }
}