﻿using Blackjack.ViewModels.Common;
using Blackjack.ViewModels.Statistic;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Services.Interfaces
{
    public interface IStatisticService
    {
        Task<RoundViewModel> GetRoundById(GetRoundStatisticViewModel requestModel);

        Task<SearchResultRoundsViewModel> GetRounds(GetSearchResultRoundsStatisticViewModel requestModel);
    }
}