﻿using Blackjack.ViewModels.Common;
using Blackjack.ViewModels.Game;
using System.Threading.Tasks;

namespace Blackjack.BusinessLogic.Services.Interfaces
{
    public interface IGameService
    {
        Task<ResponseGameInfoViewModel> Start(RequestStartGameViewModel requestModel);

        Task<RoundViewModel> Deal(DealGameViewModel requestModel);

        Task<RoundViewModel> Hit(HitGameViewModel requestModel);

        Task<RoundViewModel> Stand(StandGameViewModel requestModel);

        Task<PlayerViewModel> GetPlayerModel(GetPlayerGameViewModel requestModel);
    }
}