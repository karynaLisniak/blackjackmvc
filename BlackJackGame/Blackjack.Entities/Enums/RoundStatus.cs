﻿namespace Blackjack.Entities.Enums
{
    public enum RoundStatus
    {
        NotFinished = 1,
        PlayerWon = 2,
        DealerWon = 3,
        Blackjack = 4,
        Push = 5,
        Draw = 6
    }
}