﻿using Blackjack.Entities.Bases;
using Blackjack.Entities.Enums;

namespace Blackjack.Entities.Entities
{
    public class Card : BaseEntity
    {
        public CardSuit Suit { get; set; }

        public CardRank Rank { get; set; }

        public int Points { get; set; }
    }
}
