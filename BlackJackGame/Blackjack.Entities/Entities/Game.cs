﻿using Blackjack.Entities.Bases;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;

namespace Blackjack.Entities.Entities
{
    public class Game : BaseEntity
    {
        [Write(false)]
        public virtual ICollection<Round> Rounds { get; set; }
    }
}