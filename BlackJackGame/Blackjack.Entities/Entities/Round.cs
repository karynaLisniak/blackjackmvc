﻿using Blackjack.Entities.Bases;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using Blackjack.Entities.Enums;

namespace Blackjack.Entities.Entities
{
    public class Round : BaseEntity
    {
        public long GameId { get; set; }

        public double Rate { get; set; }

        public double Cash { get; set; }

        public RoundStatus Status { get; set; }

        [Write(false)]
        public virtual ICollection<PlayerRound> PlayerRounds { get; set; }
    }
}
