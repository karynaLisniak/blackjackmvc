﻿using Blackjack.Entities.Bases;
using Blackjack.Entities.Enums;

namespace Blackjack.Entities.Entities
{
    public class Player : BaseEntity
    {
        public PlayerRole Role { get; set; }

        public string Name { get; set; }

        public double Cash { get; set; }
    }
}
