﻿namespace Blackjack.Entities.Bases
{
    public class BaseEntity
    {
        public long Id { get; set; }
    }
}