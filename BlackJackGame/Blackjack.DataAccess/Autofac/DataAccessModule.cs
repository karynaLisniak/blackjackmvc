﻿using Autofac;
using System.Reflection;
using Module = Autofac.Module;

namespace Blackjack.DataAccess.Autofac
{
    public class DataAccessModule : Module
    {
        private string ConnectionString { get; }

        public DataAccessModule(string connectionString)
        {
            ConnectionString = connectionString;
        }

        protected override void Load(ContainerBuilder builder)
        {
            var dataAccessLayer = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(dataAccessLayer)
                .Where(t => t.Name.EndsWith("Repository"))
                .AsImplementedInterfaces()
                .WithParameter("connectionString", ConnectionString);
        }
    }
}