﻿using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Z.Dapper.Plus;

namespace Blackjack.DataAccess.Repositories
{
    public class PlayerRoundRepository : BaseRepository<PlayerRound>, IPlayerRoundRepository
    {
        public PlayerRoundRepository(string connectionString) : base(connectionString)
        {

        }

        public async Task<List<PlayerRound>> Create(IList<PlayerRound> playerRounds)
        {
            DapperPlusManager.Entity<PlayerRound>().Table("PlayerRounds").Identity(x => x.Id);

            using (TransactionScope transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (IDbConnection dbConnection = new SqlConnection(_connectionString))
                {
                    await dbConnection.BulkActionAsync(conn => conn.BulkInsert(playerRounds));

                    transaction.Complete();
                }
            }
            return playerRounds.ToList();
        }
    }
}