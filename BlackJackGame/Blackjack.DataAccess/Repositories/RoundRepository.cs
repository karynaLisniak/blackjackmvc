﻿using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Repositories
{
    public class RoundRepository : BaseRepository<Round>, IRoundRepository
    {
        public RoundRepository(string connectionString) : base(connectionString)
        {

        }

        public override async Task<Round> GetById(long id)
        {
            string sqlQuery = @"SELECT * FROM Rounds Round 
                                JOIN PlayerRounds PlayerRound ON Round.Id = PlayerRound.RoundId
                                JOIN Players Player ON PlayerRound.PlayerId = Player.Id
                                WHERE Round.Id = @id";

            Round roundResult;
            Dictionary<long, Round> dictRounds = new Dictionary<long, Round>();

            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var result = await dbConnection.QueryAsync<Round, PlayerRound, Player, Round>(
                    sqlQuery,
                    (round, playerRound, player) =>
                    {
                        if (!dictRounds.TryGetValue(round.Id, out Round queryRound))
                        {
                            queryRound = round;
                            queryRound.PlayerRounds = new List<PlayerRound>();
                            dictRounds.Add(round.Id, queryRound);
                        }
                        if (queryRound.PlayerRounds.All(source => source.Id != playerRound.Id))
                        {
                            playerRound.Player = player;
                            queryRound.PlayerRounds.Add(playerRound);
                        }
                        return queryRound;
                    },
                    new { id }
                );
                roundResult = result.FirstOrDefault();
            }
            return roundResult;
        }

        public async Task<Round> GetWithCardsById(long id)
        {
            string sqlQuery = @"SELECT * FROM Rounds Round 
                                JOIN PlayerRounds PlayerRound ON Round.Id = PlayerRound.RoundId
                                JOIN Players Player ON PlayerRound.PlayerId = Player.Id
                                JOIN Cards Card ON PlayerRound.CardId = Card.Id
                                WHERE Round.Id = @id";

            List<Round> rounds = await GetQueryRounds(sqlQuery, new { id });
            Round round = rounds.FirstOrDefault();
            return round;
        }

        public async Task<List<Round>> GetByCount(int startPosition = 0, int count = 0, string playerName = "")
        {
            string sqlQuery = $@"SELECT * FROM Rounds Round 
                                 JOIN PlayerRounds PlayerRound ON Round.Id = PlayerRound.RoundId
                                 JOIN Players Player ON PlayerRound.PlayerId = Player.Id
								 JOIN Cards Card ON Card.Id = PlayerRound.CardId
                                 WHERE Round.Id IN (SELECT DISTINCT R.Id FROM Rounds R
                                    JOIN PlayerRounds PR ON PR.RoundId = R.Id
                                    JOIN Players P ON P.Id = PR.PlayerId
                                    WHERE P.Name LIKE CONCAT('%',@playerName,'%')
                                    ORDER BY R.Id
                                    OFFSET { startPosition } ROWS FETCH NEXT { count } ROWS ONLY)";

            List<Round> rounds = await GetQueryRounds(sqlQuery, new { playerName });
            return rounds;
        }

        public async Task<int> GetRoundsCount(string playerName = "")
        {
            string sqlQuery = @"SELECT Count(Round.Id) FROM Rounds Round 
                                 WHERE Round.Id IN (SELECT R.Id FROM Rounds R
                                    JOIN PlayerRounds PR ON PR.RoundId = R.Id
                                    JOIN Players P ON P.Id = PR.PlayerId
                                    WHERE P.Name LIKE CONCAT('%',@playerName,'%'))";

            int countRounds;

            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                countRounds = await dbConnection.QueryFirstAsync<int>(sqlQuery, new { playerName });
            }
            return countRounds;
        }

        private async Task<List<Round>> GetQueryRounds(string sqlRequestQuery, object parametr)
        {
            List<Round> rounds;
            Dictionary<long, Round> dictRounds = new Dictionary<long, Round>();

            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                var result = await dbConnection.QueryAsync<Round, PlayerRound, Player, Card, Round>(
                    sqlRequestQuery,
                    (round, playerRound, player, card) =>
                    {
                        if (!dictRounds.TryGetValue(round.Id, out Round queryRound))
                        {
                            queryRound = round;
                            queryRound.PlayerRounds = new List<PlayerRound>();
                            dictRounds.Add(round.Id, queryRound);
                        }
                        if (queryRound.PlayerRounds.All(source => source.Id != playerRound.Id))
                        {
                            playerRound.Player = player;
                            playerRound.Card = card;
                            queryRound.PlayerRounds.Add(playerRound);
                        }
                        return queryRound;
                    },
                    parametr
                );
                rounds = result.Distinct().ToList();
            }
            return rounds;
        }
    }
}