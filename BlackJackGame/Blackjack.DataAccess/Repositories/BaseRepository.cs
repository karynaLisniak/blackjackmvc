﻿using Blackjack.DataAccess.Repositories.Interfaces;
using Dapper.Contrib.Extensions;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Blackjack.Entities.Bases;

namespace Blackjack.DataAccess.Repositories
{
    public class BaseRepository<TBaseEntity> : IBaseRepository<TBaseEntity>
        where TBaseEntity : BaseEntity
    {
        protected readonly string _connectionString;

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public virtual async Task<TBaseEntity> Create(TBaseEntity item)
        {
            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                item.Id = await dbConnection.InsertAsync(item);
            }
            return item;
        }

        public virtual async Task<TBaseEntity> GetById(long id)
        {
            TBaseEntity item;

            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                item = await connection.GetAsync<TBaseEntity>(id);
            }
            return item;
        }

        public virtual async Task Update(TBaseEntity item)
        {
            using (IDbConnection connection = new SqlConnection(_connectionString))
            {
                await connection.UpdateAsync(item);
            }
        }
    }
}