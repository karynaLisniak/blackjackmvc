﻿using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using Blackjack.Entities.Enums;
using Dapper;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Z.Dapper.Plus;

namespace Blackjack.DataAccess.Repositories
{
    public class PlayerRepository : BaseRepository<Player>, IPlayerRepository
    {
        public PlayerRepository(string connectionString) : base(connectionString)
        {

        }

        public async Task<List<Player>> Create(IList<Player> players)
        {
            DapperPlusManager.Entity<Player>().Table("Players").Identity(x => x.Id);

            using (TransactionScope transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (IDbConnection dbConnection = new SqlConnection(_connectionString))
                {
                    await dbConnection.BulkActionAsync(conn => conn.BulkInsert(players));

                    transaction.Complete();
                }
            }
            return players.ToList();
        }


        public async Task<Player> GetByNameAndRole(string playerName, PlayerRole role = PlayerRole.User)
        {
            string sqlQuery = "SELECT * FROM Players WHERE Name = @playerName AND Role = @role";

            Player player;

            using (IDbConnection dbConnection = new SqlConnection(_connectionString))
            {
                player = await dbConnection.QueryFirstOrDefaultAsync<Player>(sqlQuery, new { playerName, role });
            }
            return player;
        }
    }
}