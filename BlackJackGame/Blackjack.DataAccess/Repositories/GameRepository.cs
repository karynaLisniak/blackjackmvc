﻿using Blackjack.DataAccess.Repositories.Interfaces;
using Blackjack.Entities.Entities;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using System.Transactions;

namespace Blackjack.DataAccess.Repositories
{
    public class GameRepository : BaseRepository<Game>, IGameRepository
    {
        public GameRepository(string connectionString) : base(connectionString)
        {

        }

        public new async Task<Game> Create(Game game)
        {
            string sqlQuery = @"INSERT INTO Games DEFAULT VALUES; 
                                SELECT * FROM Games WHERE Id = CAST(SCOPE_IDENTITY() as int)";

            using (TransactionScope transaction = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                using (IDbConnection dbConnection = new SqlConnection(_connectionString))
                {
                    game = await dbConnection.QueryFirstOrDefaultAsync<Game>(sqlQuery, game);

                    transaction.Complete();
                }
            }
            return game;
        }
    }
}