﻿using Blackjack.Entities.Entities;
using Blackjack.Entities.Enums;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Repositories.Interfaces
{
    public interface IPlayerRepository
    {
        Task<Player> Create(Player player);

        Task<List<Player>> Create(IList<Player> players);

        Task<Player> GetById(long id);

        Task<Player> GetByNameAndRole(string playerName, PlayerRole role = PlayerRole.User);

        Task Update(Player player);
    }
}