﻿using Blackjack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Repositories.Interfaces
{
    public interface IRoundRepository
    {
        Task<Round> Create(Round round);

        Task<Round> GetById(long id);

        Task<Round> GetWithCardsById(long id);

        Task<List<Round>> GetByCount(int startPosition = 0, int count = 0, string playerName = "");

        Task<int> GetRoundsCount(string playerName = "");

        Task Update(Round round);
    }
}