﻿using System.Threading.Tasks;
using Blackjack.Entities.Bases;

namespace Blackjack.DataAccess.Repositories.Interfaces
{
    public interface IBaseRepository<TBaseEntity> 
        where TBaseEntity : BaseEntity
    {
        Task<TBaseEntity> Create(TBaseEntity item);

        Task<TBaseEntity> GetById(long id);

        Task Update(TBaseEntity item);
    }
}