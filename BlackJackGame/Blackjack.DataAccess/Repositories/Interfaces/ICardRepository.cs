﻿using Blackjack.Entities.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Blackjack.DataAccess.Repositories.Interfaces
{
    public interface ICardRepository
    {
        Task<List<Card>> GetFreeCards(long roundId);
    }
}