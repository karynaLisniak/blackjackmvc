﻿using System.Collections.Generic;

namespace Blackjack.ViewModels.Common
{
    public class RoundViewModel
    {
        public long RoundId { get; set; }

        public long GameId { get; set; }

        public PlayerViewModel Player { get; set; }

        public PlayerViewModel Dealer { get; set; }

        public List<PlayerViewModel> Bots { get; set; }

        public string Status { get; set; }

        public RoundViewModel()
        {
            Bots = new List<PlayerViewModel>();
        }
    }
}