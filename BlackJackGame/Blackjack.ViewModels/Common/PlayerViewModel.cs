﻿using System.Collections.Generic;

namespace Blackjack.ViewModels.Common
{
    public class PlayerViewModel
    {
        public long Id { get; set; }

        public string Role { get; set; }

        public string Name { get; set; }

        public double Cash { get; set; }

        public double Rate { get; set; }

        public int Score { get; set; }

        public IList<CardViewModel> Cards { get; set; }

        public PlayerViewModel()
        {
            Cards = new List<CardViewModel>();
        }
    }
}