﻿namespace Blackjack.ViewModels.Statistic
{
    public class GetSearchResultRoundsStatisticViewModel
    {
        public int StartPosition { get; set; }

        public int CountRounds { get; set; }

        public string PlayerName { get; set; }
    }
}