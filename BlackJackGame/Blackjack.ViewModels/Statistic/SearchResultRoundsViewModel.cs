﻿using Blackjack.ViewModels.Common;
using System.Collections.Generic;

namespace Blackjack.ViewModels.Statistic
{
    public class SearchResultRoundsViewModel
    {
        public IList<RoundViewModel> Rounds { get; set; }

        public int CountRounds { get; set; }
    }
}