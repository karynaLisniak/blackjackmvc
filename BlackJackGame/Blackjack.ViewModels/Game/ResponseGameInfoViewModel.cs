﻿namespace Blackjack.ViewModels.Game
{
    public class ResponseGameInfoViewModel
    {
        public long GameId { get; set; }

        public long PlayerId { get; set; }
    }
}