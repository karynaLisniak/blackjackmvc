﻿namespace Blackjack.ViewModels.Game
{
    public class RequestStartGameViewModel
    {
        public string Name { get; set; }

        public double Cash { get; set; }
    }
}