﻿namespace Blackjack.ViewModels.Game
{
    public class DealGameViewModel
    {
        public long GameId { get; set; }

        public long PlayerId { get; set; }

        public int CountBots { get; set; }

        public double Rate { get; set; }
    }
}